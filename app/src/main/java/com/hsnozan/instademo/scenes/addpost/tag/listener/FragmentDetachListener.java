package com.hsnozan.instademo.scenes.addpost.tag.listener;

import com.hsnozan.instademo.scenes.addpost.tag.model.TaggedPeople;

import java.util.ArrayList;

/**
 * Created by ozanal on 2019-05-25
 */
public interface FragmentDetachListener {

    void onFragmentDetached(ArrayList<TaggedPeople> taggedPeopleList);

}
