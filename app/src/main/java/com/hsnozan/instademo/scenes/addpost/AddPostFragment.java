package com.hsnozan.instademo.scenes.addpost;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.hsnozan.instademo.R;
import com.hsnozan.instademo.scenes.addpost.tag.TagFragment;
import com.hsnozan.instademo.scenes.addpost.tag.listener.FragmentDetachListener;
import com.hsnozan.instademo.scenes.addpost.tag.model.TaggedPeople;
import com.hsnozan.instademo.scenes.home.activity.HomeActivity;
import com.hsnozan.instademo.scenes.home.scene.homepage.model.HomePostListItem;
import com.hsnozan.instademo.util.FragmentUtil;
import com.hsnozan.instademo.util.GTFrameLayout;
import com.hsnozan.instademo.util.ImagePicker;
import com.hsnozan.instademo.util.Util;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;

import static android.app.Activity.RESULT_OK;
import static android.support.constraint.Constraints.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddPostFragment extends Fragment implements EasyPermissions.PermissionCallbacks, ValueEventListener, FragmentDetachListener {

    private static final int QUALITY = 92;
    private static final int MAX_PROFILE_BITMAP_SIZE = 1024;
    public static final int REQUEST_CODE_PICKER_IMAGE = 777;
    public static final int PERMISSION_CODE_STORAGE = 444;

    @BindView(R.id.frame_layout_add_post)
    GTFrameLayout rootView;
    @BindView(R.id.dummy_image)
    AppCompatImageView postImage;
    @BindView(R.id.upload_image_button)
    AppCompatButton uploadButton;
    @BindView(R.id.edit_text_post_detail)
    AppCompatEditText postDetailEditText;
    @BindView(R.id.text_post_tag_people)
    AppCompatTextView tagPeopleText;
    @BindView(R.id.text_post_tag_people_count)
    AppCompatTextView taggedPeopleCountText;
    @BindView(R.id.card_view_tag_people_container)
    CardView cardView;

    private ArrayList<TaggedPeople> taggedPeopleList = new ArrayList<>();
    private FirebaseAuth firebaseAuth;
    private DatabaseReference reference;
    private DatabaseReference newPost;
    private Uri filePath;
    private Uri downloadUri;
    private String postDetailText;
    private Bitmap profileBitmap;

    public AddPostFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_post, container, false);
        ButterKnife.bind(this, view);
        firebaseAuth = FirebaseAuth.getInstance();

        setFirebaseReference();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        reference.removeEventListener(this);
    }

    private void setFirebaseReference() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        reference = database
                .getReferenceFromUrl("https://instademo-2409f.firebaseio.com//postList/postListItem");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_PICKER_IMAGE && resultCode == RESULT_OK) {
            final Uri imageUri = ImagePicker.getImageFromResult(getContext(), data);
            startCropping(imageUri);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                filePath = result.getUri();
                try {
                    InputStream inputStream = getActivity().getContentResolver()
                            .openInputStream(filePath);
                    profileBitmap = BitmapFactory.decodeStream(inputStream);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    profileBitmap = getResizedBitmap(profileBitmap, MAX_PROFILE_BITMAP_SIZE);
                    profileBitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY,
                            byteArrayOutputStream);
                    postImage.setImageBitmap(profileBitmap);
                    cardView.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @OnClick(R.id.dummy_image)
    public void onChooseButtonClick() {
        openCameraForProfilePicture();
    }

    @OnClick(R.id.upload_image_button)
    public void onUploadButtonClick() {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReference();
        postDetailText = postDetailEditText.getText().toString();
        if (filePath != null && (!postDetailText.equals(""))) {
            rootView.showProgress();
            final StorageReference ref = storageReference.child("images/" + UUID.randomUUID().toString());
            ref.putFile(filePath)
                    .addOnSuccessListener(taskSnapshot -> ref.getDownloadUrl()
                            .addOnSuccessListener(uri -> {
                                downloadUri = uri;
                                newPost = reference.push();
                                referenceValueListener();
                            }))
                    .addOnFailureListener(e -> Toast.makeText(getContext(), "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show());
        } else {
            Toast.makeText(getContext(), "Image and Description can not be empty ", Toast.LENGTH_SHORT).show();
        }
    }

    private void referenceValueListener() {
        reference.addValueEventListener(this);
    }

    private void openCameraForProfilePicture() {
        String[] permissions;
        permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (!hasPermissions(getContext(), permissions)) {
            onCameraRequested();
        } else {
            requestCamera();
        }
    }

    private boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission)
                        != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void onCameraRequested() {
        if (getActivity() == null) {
            Log.w(TAG, "onCameraRequested: context is null");
            return;
        }

        String[] perms;
        perms = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        if (EasyPermissions.hasPermissions(getActivity(), perms)) {
            requestCamera();
        } else {
            EasyPermissions.requestPermissions(new PermissionRequest
                    .Builder(this, PERMISSION_CODE_STORAGE, perms)
                    .setRationale(R.string.text_need_storage_access)
                    .setPositiveButtonText(R.string.text_ok)
                    .setNegativeButtonText(R.string.text_cancel)
                    .build());
        }
    }

    @AfterPermissionGranted(PERMISSION_CODE_STORAGE)
    private void requestCamera() {
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(getContext());
        startActivityForResult(chooseImageIntent, REQUEST_CODE_PICKER_IMAGE);
    }

    private void startCropping(Uri imageUri) {
        CropImage.ActivityBuilder builder = CropImage.activity(imageUri)
                .setAllowRotation(false)
                .setFixAspectRatio(true)
                .setFlipHorizontally(false)
                .setAllowFlipping(false)
                .setMultiTouchEnabled(true)
                .setCropShape(CropImageView.CropShape.RECTANGLE);

        builder.start(getContext(), this);
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions,
                grantResults, this);
    }

    public int getContainer() {
        return R.id.frame_layout_add_post;
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        HomePostListItem homePostListItem = new HomePostListItem();
        homePostListItem.imageUrl = downloadUri.toString();
        homePostListItem.postDescription = postDetailText;
        homePostListItem.userName = firebaseAuth.getCurrentUser().getDisplayName();
        if (taggedPeopleList.size() != 0) {
            homePostListItem.taggedPeople = taggedPeopleList;
        }
        newPost.setValue(homePostListItem)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        rootView.hideProgress();
                        HomeActivity activity = (HomeActivity) getActivity();

                        if (activity != null) {
                            FragmentUtil.popLatestFragment(activity);
                            Util.closeSoftKeyboard(activity);
                            activity.setTab(0);
                        }
                    }
                });
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    @OnClick(R.id.card_view_tag_people_container)
    public void onTagPeopleClick() {
        if (filePath != null) {
            HomeActivity activity = (HomeActivity) getActivity();

            if (activity != null) {
                TagFragment tagFragment = TagFragment.newInstance(taggedPeopleList);
                tagFragment.setBitmap(profileBitmap);
                tagFragment.setDetachListener(this);
                FragmentUtil.startFragment(activity, activity.getContainer(), tagFragment);
            }
        }
    }

    @Override
    public void onFragmentDetached(ArrayList<TaggedPeople> taggedPeopleList) {
        this.taggedPeopleList = taggedPeopleList;
        String taggedPeople = getResources().getString(R.string.text_tagged_count,
                taggedPeopleList.size());
        taggedPeopleCountText.setText(taggedPeople);
    }
}
