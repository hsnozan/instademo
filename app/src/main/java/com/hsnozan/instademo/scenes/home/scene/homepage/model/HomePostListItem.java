package com.hsnozan.instademo.scenes.home.scene.homepage.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.hsnozan.instademo.scenes.addpost.tag.model.TaggedPeople;
import com.hsnozan.instademo.scenes.home.scene.comments.model.CommentsModel;

import java.util.ArrayList;

/**
 * Created by ozanal on 2019-05-24
 */
public class HomePostListItem implements Parcelable {

    public String userName;
    public String imageUrl;
    public String postDescription;
    public ArrayList<CommentsModel> postComments;
    public ArrayList<TaggedPeople> taggedPeople;
    private String postID;

    public HomePostListItem() {

    }

    protected HomePostListItem(Parcel in) {
        userName = in.readString();
        imageUrl = in.readString();
        postDescription = in.readString();
        postID = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(imageUrl);
        dest.writeString(postDescription);
        dest.writeString(postID);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<HomePostListItem> CREATOR = new Creator<HomePostListItem>() {
        @Override
        public HomePostListItem createFromParcel(Parcel in) {
            return new HomePostListItem(in);
        }

        @Override
        public HomePostListItem[] newArray(int size) {
            return new HomePostListItem[size];
        }
    };

    public void setPostID(String key) {
        this.postID = key;
    }

    public String getPostID() {
        return postID;
    }
}
