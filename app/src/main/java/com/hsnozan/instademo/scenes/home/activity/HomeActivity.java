package com.hsnozan.instademo.scenes.home.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.hsnozan.instademo.R;
import com.hsnozan.instademo.scenes.addpost.AddPostFragment;
import com.hsnozan.instademo.scenes.explore.ExploreFragment;
import com.hsnozan.instademo.scenes.home.activity.adapter.ViewPagerAdapter;
import com.hsnozan.instademo.scenes.home.scene.homepage.HomeFragment;
import com.hsnozan.instademo.util.FragmentUtil;
import com.hsnozan.instademo.util.view.HomeNavigationView;
import com.hsnozan.instademo.util.view.NonAnimatedViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.hsnozan.instademo.util.view.HomeNavigationView.POSITION_FIRST;
import static com.hsnozan.instademo.util.view.HomeNavigationView.POSITION_LAST;
import static com.hsnozan.instademo.util.view.HomeNavigationView.POSITION_SCAN;

public class HomeActivity extends AppCompatActivity implements HomeNavigationView.OnItemSelectionListener {

    @BindView(R.id.main_view_pager)
    NonAnimatedViewPager viewPager;
    @BindView(R.id.home_navigation_view)
    HomeNavigationView homeNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        setUpViewPager();
    }

    private void setUpViewPager() {
        List<Fragment> mFragments = new ArrayList<>();
        HomeFragment homeFragment = new HomeFragment();
        mFragments.add(homeFragment);
        mFragments.add(new ExploreFragment());

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), mFragments);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setOffscreenPageLimit(mFragments.size());

        homeNavigationView.setItemSelectionListener(this);
    }

    public int getContainer() {
        return R.id.frame_layout_container;
    }

    public void setTab(int position) {
        viewPager.setCurrentItem(position);
    }

    @Override
    public void onItemSelected(int itemPosition) {
        switch (itemPosition) {
            case POSITION_LAST:
                viewPager.setCurrentItem(POSITION_LAST - 1);
                break;
            case POSITION_SCAN:
                startAddPostFragment();
                break;
            case POSITION_FIRST:
            default:
                viewPager.setCurrentItem(POSITION_FIRST);
                break;
        }
    }

    private void startAddPostFragment() {
        AddPostFragment addPostFragment = new AddPostFragment();
        FragmentUtil.startFragment(this, getContainer(), addPostFragment);
    }
}
