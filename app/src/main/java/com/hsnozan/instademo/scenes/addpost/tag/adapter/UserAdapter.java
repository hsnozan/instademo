package com.hsnozan.instademo.scenes.addpost.tag.adapter;

import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hsnozan.instademo.R;
import com.hsnozan.instademo.scenes.addpost.tag.TagFragment;
import com.hsnozan.instademo.scenes.addpost.tag.listener.UserItemClickListener;
import com.hsnozan.instademo.scenes.login.model.UserModel;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ozanal on 2019-05-25
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private ArrayList<UserModel> userModelArrayList = new ArrayList<>();
    private UserItemClickListener itemClickListener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_tag_user_item, parent,
                        false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(sortedList.get(i));
    }

    @Override
    public int getItemCount() {
        return sortedList.size();
    }

    public void setUserListArrayList(ArrayList<UserModel> userModels) {
        this.userModelArrayList = userModels;
        notifyDataSetChanged();
    }

    public void setListener(UserItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    private static final Comparator<UserModel> COMPARATOR = (a, b) -> {
        Locale trLocale = new Locale("tr", "TR");
        final Collator collator = Collator.getInstance(trLocale);

        return collator.compare(a.userName, b.userName);
    };

    final SortedList<UserModel> sortedList = new SortedList<>(UserModel.class,
            new SortedList.Callback<UserModel>() {

                @Override
                public void onInserted(int position, int count) {
                    notifyItemRangeInserted(position, count);
                }

                @Override
                public void onRemoved(int position, int count) {
                    notifyItemRangeRemoved(position, count);
                }

                @Override
                public void onMoved(int fromPosition, int toPosition) {
                    notifyItemMoved(fromPosition, toPosition);
                }

                @Override
                public int compare(UserModel a, UserModel b) {
                    return COMPARATOR.compare(a, b);
                }

                @Override
                public void onChanged(int position, int count) {
                    notifyItemRangeChanged(position, count);
                }

                @Override
                public boolean areContentsTheSame(UserModel oldItem,
                                                  UserModel newItem) {
                    return oldItem.equals(newItem);
                }

                @Override
                public boolean areItemsTheSame(UserModel item1,
                                               UserModel item2) {
                    return item1 == item2;
                }
            });

    public void replaceAll(ArrayList<UserModel> filteredList) {
        sortedList.clear();
        sortedList.beginBatchedUpdates();

        for (int i = sortedList.size() - 1; i >= 0; i--) {
            final UserModel model = sortedList.get(i);
            if (!filteredList.contains(model)) {
                filteredList.remove(model);
            }
        }

        sortedList.addAll(filteredList);

        notifyDataSetChanged();
        sortedList.endBatchedUpdates();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.text_user_item_name)
        AppCompatTextView userName;

        private String userNameText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        public void bind(UserModel userModel) {
            this.userNameText = userModel.userName;
            userName.setText(userNameText);
        }

        @Override
        public void onClick(View view) {
            if (itemClickListener != null) {
                itemClickListener.onUserItemClickListener(userNameText);
            }
        }
    }
}
