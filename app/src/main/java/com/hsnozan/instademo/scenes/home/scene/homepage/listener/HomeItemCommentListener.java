package com.hsnozan.instademo.scenes.home.scene.homepage.listener;

import android.graphics.Bitmap;

import com.hsnozan.instademo.scenes.home.scene.homepage.model.HomePostListItem;

/**
 * Created by ozanal on 2019-05-24
 */
public interface HomeItemCommentListener {

    void onCommentButtonClick(HomePostListItem listItem);

    void onSaveButtonClick(Bitmap bitmap);

}
