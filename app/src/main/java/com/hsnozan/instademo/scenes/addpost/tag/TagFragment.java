package com.hsnozan.instademo.scenes.addpost.tag;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hsnozan.instademo.R;
import com.hsnozan.instademo.scenes.addpost.tag.adapter.UserAdapter;
import com.hsnozan.instademo.scenes.addpost.tag.listener.FragmentDetachListener;
import com.hsnozan.instademo.scenes.addpost.tag.listener.UserItemClickListener;
import com.hsnozan.instademo.scenes.addpost.tag.model.TaggedPeople;
import com.hsnozan.instademo.scenes.home.activity.HomeActivity;
import com.hsnozan.instademo.scenes.login.model.UserModel;
import com.hsnozan.instademo.util.FragmentUtil;
import com.hsnozan.instademo.util.GTFrameLayout;
import com.hsnozan.instademo.util.Util;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class TagFragment extends Fragment implements View.OnTouchListener, View.OnClickListener, ValueEventListener, UserItemClickListener, TextWatcher {

    private static final String KEY_TAGGED_PEOPLE = "KEY_TAGGED_PEOPLE";

    @BindView(R.id.view_image_container)
    GTFrameLayout imageContainerView;
    @BindView(R.id.layout_choose_action_container)
    ConstraintLayout chooseActionContainer;
    @BindView(R.id.edit_text_choose_user)
    AppCompatEditText chooseUserEditText;
    @BindView(R.id.image_by_user)
    AppCompatImageView imageByUser;
    @BindView(R.id.recycler_view_user)
    RecyclerView userRecyclerView;

    private ArrayList<TaggedPeople> taggedPeopleList = new ArrayList<>();
    private ArrayList<UserModel> userModelArrayList;
    private Bitmap bitmap;
    private ViewGroup view;
    private View taggedView;
    private AppCompatTextView taggedUser;
    private AppCompatImageView removeTagButton;
    private DatabaseReference reference;
    private UserAdapter userAdapter;
    private FragmentDetachListener detachListener;
    private boolean isTaggedViewAdded = false;
    private int x;
    private int y;

    public TagFragment() {
        // Required empty public constructor
    }

    public static TagFragment newInstance(ArrayList<TaggedPeople> taggedPeopleList) {
        TagFragment tagFragment = new TagFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_TAGGED_PEOPLE, taggedPeopleList);
        tagFragment.setArguments(args);
        return tagFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = (ViewGroup) inflater.inflate(R.layout.fragment_tag, container, false);
        ButterKnife.bind(this, view);

        setUpAdapter();
        setUpImageView();
        setUpFirebaseDatabase();

        if (getArguments() != null && getArguments().getParcelableArrayList(KEY_TAGGED_PEOPLE) != null) {
            ArrayList<TaggedPeople> taggedPeople = getArguments().getParcelableArrayList(KEY_TAGGED_PEOPLE);
            if (taggedPeople != null) {
                for (int i = 0; i < taggedPeople.size(); i++) {
                    addLayoutFromAddPost(taggedPeople.get(i));
                }
            }
        }
        imageByUser.setOnTouchListener(this);
        chooseUserEditText.addTextChangedListener(this);
        return view;
    }

    private void setUpAdapter() {
        userAdapter = new UserAdapter();
        userAdapter.setListener(this);
        userRecyclerView.setAdapter(userAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        userRecyclerView.setLayoutManager(linearLayoutManager);
    }

    private void setUpFirebaseDatabase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        reference = database.getReferenceFromUrl("https://instademo-2409f.firebaseio.com//userList/user");
        reference.addValueEventListener(this);
    }

    private void setUpImageView() {
        if (bitmap != null) {
            imageByUser.setImageBitmap(bitmap);
        }
    }

    public void setBitmap(Bitmap profileBitmap) {
        this.bitmap = profileBitmap;
    }

    @Override
    public boolean onTouch(View view1, MotionEvent motionEvent) {
        chooseUserEditText.setVisibility(View.VISIBLE);
        x = (int) motionEvent.getX();
        y = (int) motionEvent.getY();

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (!isTaggedViewAdded) {
                    if (taggedView != null) {
                        imageContainerView.removeView(taggedView);
                    }
                } else {
                    isTaggedViewAdded = false;
                }

                addLayout(x, y);
                removeTagButton.setVisibility(View.GONE);
                chooseUserEditText.requestFocus();
                Util.showSoftKeyboard(getActivity());
                break;
            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
        }
        return false;
    }

    private void addLayout(int x, int y) {
        taggedView = LayoutInflater.from(getContext()).inflate(R.layout.view_photo_tag, view, false);
        taggedUser = taggedView.findViewById(R.id.text_photo_tag_user_name);
        removeTagButton = taggedView.findViewById(R.id.image_remove_tag);
        taggedUser.setOnClickListener(this);
        removeTagButton.setOnClickListener(this);

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) taggedView.getLayoutParams();
        params.leftMargin = x - 20;
        params.topMargin = y - 20;
        taggedView.setLayoutParams(params);
        imageContainerView.addView(taggedView);
    }


    private void addLayoutFromAddPost(TaggedPeople taggedPeople) {
        taggedView = LayoutInflater.from(getContext()).inflate(R.layout.view_photo_tag, view, false);
        taggedUser = taggedView.findViewById(R.id.text_photo_tag_user_name);
        removeTagButton = taggedView.findViewById(R.id.image_remove_tag);
        taggedUser.setOnClickListener(this);
        removeTagButton.setOnClickListener(this);
        taggedUser.setText(taggedPeople.taggedPerson);

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) taggedView.getLayoutParams();
        params.leftMargin = taggedPeople.x;
        params.topMargin = taggedPeople.y;
        taggedView.setLayoutParams(params);
        imageContainerView.addView(taggedView);
    }

    @Override
    public void onClick(View view) {
        if (view == taggedUser) {
            if (removeTagButton.getVisibility() == View.GONE) {
                removeTagButton.setVisibility(View.VISIBLE);
            } else {
                removeTagButton.setVisibility(View.GONE);
            }
        } else if (view == removeTagButton) {
            imageContainerView.removeView(taggedView);
            chooseUserEditText.clearFocus();
            chooseUserEditText.setVisibility(View.GONE);
            Util.closeSoftKeyboard(getActivity());
        }
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<UserModel> userModels = new ArrayList<>();

        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            UserModel userModel = snapshot.getValue(UserModel.class);
            userModels.add(userModel);
        }

        userModelArrayList = userModels;
        userAdapter.replaceAll(userModels);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    @Override
    public void onUserItemClickListener(String userName) {
        if (chooseUserEditText.getText() != null) {
            chooseUserEditText.getText().clear();
        }
        chooseUserEditText.setVisibility(View.GONE);
        taggedUser.setText(userName);
        userRecyclerView.setVisibility(View.GONE);
        onConfirmTagAdded();
    }

    private void onConfirmTagAdded() {
        if (!isTaggedViewAdded) {
            TaggedPeople taggedPeople = new TaggedPeople();
            taggedPeople.taggedPerson = taggedUser.getText().toString();
            taggedPeople.x = x;
            taggedPeople.y = y;
            taggedPeopleList.add(taggedPeople);
        }
        isTaggedViewAdded = true;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        userRecyclerView.setVisibility(View.VISIBLE);
        onFilterQueryChange(charSequence.toString());
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @OnClick(R.id.image_tag_cancel)
    public void onCancelButtonClick() {
        closeFragment(false);
    }

    @OnClick(R.id.image_tag_confirm)
    public void onConfirmButtonClick() {
        closeFragment(true);
    }

    private void closeFragment(boolean isTagAdded) {
        if (!isTagAdded) {
            detachListener = null;
        }

        HomeActivity activity = (HomeActivity) getActivity();

        if (activity != null) {
            FragmentUtil.popLatestFragment(activity);
        }
    }

    public void setDetachListener(FragmentDetachListener fragmentDetachListener) {
        this.detachListener = fragmentDetachListener;
    }

    private void onFilterQueryChange(String newQuery) {
        final ArrayList<UserModel> filteredModelList = filter(newQuery);
        userAdapter.replaceAll(filteredModelList);
    }

    private ArrayList<UserModel> filter(String query) {
        Locale locale = new Locale("tr", "TR");
        final String lowerCaseQuery = query.toLowerCase(locale);
        final ArrayList<UserModel> filteredList = new ArrayList<>();

        for (UserModel userModel : userModelArrayList) {
            final String text = userModel
                    .userName.toLowerCase(locale);

            if (text.contains(lowerCaseQuery)) {
                UserModel newUserModel
                        = new UserModel();
                newUserModel.userName = text;
                filteredList.add(newUserModel);
            }
        }

        return filteredList;
    }

    @Override
    public void onDetach() {
        if (detachListener != null && taggedPeopleList.size() != 0) {
            detachListener.onFragmentDetached(taggedPeopleList);
        }
        super.onDetach();
    }
}
