package com.hsnozan.instademo.scenes.home.scene.homepage.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.hsnozan.instademo.R;
import com.hsnozan.instademo.scenes.addpost.tag.model.TaggedPeople;
import com.hsnozan.instademo.scenes.home.scene.homepage.listener.HomeItemCommentListener;
import com.hsnozan.instademo.scenes.home.scene.homepage.model.HomePostListItem;
import com.hsnozan.instademo.util.Util;

import java.util.ArrayList;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ozanal on 2019-05-24
 */
public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private static final String TAG = "";
    private ArrayList<HomePostListItem> postListItem = new ArrayList<>();
    private HomeItemCommentListener itemListener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_home_list_item, parent,
                        false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(postListItem.get(i));
    }

    @Override
    public int getItemCount() {
        return postListItem.size();
    }

    public void setPostListArrayList(ArrayList<HomePostListItem> postListItem) {
        this.postListItem = postListItem;
        notifyDataSetChanged();
    }

    public void setListener(HomeItemCommentListener itemCommentListener) {
        this.itemListener = itemCommentListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnTouchListener {

        @BindView(R.id.text_user_name)
        AppCompatTextView userName;
        @BindView(R.id.image_like_button)
        AppCompatImageView likeButton;
        @BindView(R.id.image_posted)
        PhotoView postedImage;
        @BindView(R.id.text_view_all_comments)
        AppCompatTextView allCommentsText;
        @BindView(R.id.text_view_user_description)
        AppCompatTextView userDescription;
        @BindView(R.id.image_has_tags)
        AppCompatImageView imageHasTags;
        @BindView(R.id.view_group)
        FrameLayout viewGroup;

        @BindDrawable(R.drawable.ic_star)
        Drawable icStar;
        @BindDrawable(R.drawable.ic_star_selected)
        Drawable icStarSelected;

        private boolean isImgLiked = false;
        private HomePostListItem listItem;
        private Bitmap bitmap;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(HomePostListItem homePostListItem) {
            this.listItem = homePostListItem;
            this.bitmap = Util.getImageData(homePostListItem.imageUrl);
            String userNameDescription = "<b>" + homePostListItem.userName + "</b>";
            String fullDescription = userNameDescription + " " + homePostListItem.postDescription;
            userName.setText(homePostListItem.userName);
            userDescription.setText(Html.fromHtml(fullDescription));

            Glide.with(itemView).load(homePostListItem.imageUrl).into(postedImage);
            postedImage.setScaleType(ImageView.ScaleType.CENTER_CROP);

            if (homePostListItem.postComments != null && homePostListItem.postComments.size() != 0) {
                allCommentsText.setVisibility(View.VISIBLE);
            } else {
                allCommentsText.setVisibility(View.GONE);
            }

            if (homePostListItem.taggedPeople != null) {
                imageHasTags.setVisibility(View.VISIBLE);
            }

            viewGroup.setOnTouchListener(this);
        }

        @OnClick(R.id.image_like_button)
        public void onLikeButtonClick() {
            isImgLiked = !isImgLiked;
            likeButton.setImageDrawable(isImgLiked ? icStarSelected : icStar);
        }

        @OnClick({R.id.image_comment_button, R.id.text_view_all_comments})
        public void onCommentButtonClick() {
            if (itemListener != null) {
                itemListener.onCommentButtonClick(listItem);
            }
        }

        @OnClick(R.id.image_save_button)
        public void onSaveButtonClick() {
            if (itemListener != null) {
                itemListener.onSaveButtonClick(bitmap);
            }
        }

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (listItem.taggedPeople != null) {
                if (imageHasTags.getVisibility() == View.VISIBLE) {
                    for (int i = 0; i < listItem.taggedPeople.size(); i++) {
                        addLayout(listItem.taggedPeople.get(i));
                    }
                    imageHasTags.setVisibility(View.GONE);
                } else {
                    viewGroup.removeAllViews();
                    imageHasTags.setVisibility(View.VISIBLE);
                }
            }
            return false;
        }

        private void addLayout(TaggedPeople taggedPeople) {
            View taggedView = LayoutInflater.from(itemView.getContext()).inflate(R.layout.view_photo_tag, viewGroup, false);
            AppCompatTextView taggedUser = taggedView.findViewById(R.id.text_photo_tag_user_name);

            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) taggedView.getLayoutParams();
            params.leftMargin = taggedPeople.x - 20;
            params.topMargin = taggedPeople.y - 20;
            taggedView.setLayoutParams(params);
            taggedUser.setText(taggedPeople.taggedPerson);
            viewGroup.addView(taggedView);
        }
    }
}
