package com.hsnozan.instademo.scenes.explore.adapter;

import android.support.annotation.NonNull;
import android.support.v7.util.SortedList;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.hsnozan.instademo.R;
import com.hsnozan.instademo.scenes.home.scene.homepage.model.HomePostListItem;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ozanal on 2019-05-24
 */
public class ExploreAdapter extends RecyclerView.Adapter<ExploreAdapter.ViewHolder> {

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_explore_grid_item, parent,
                        false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(sortedList.get(i));
    }

    @Override
    public int getItemCount() {
        return sortedList.size();
    }

    private static final Comparator<HomePostListItem> COMPARATOR = (a, b) -> {
        Locale trLocale = new Locale("tr", "TR");
        final Collator collator = Collator.getInstance(trLocale);

        return collator.compare(a.userName, b.userName);
    };

    final SortedList<HomePostListItem> sortedList = new SortedList<>(HomePostListItem.class,
            new SortedList.Callback<HomePostListItem>() {

                @Override
                public void onInserted(int position, int count) {
                    notifyItemRangeInserted(position, count);
                }

                @Override
                public void onRemoved(int position, int count) {
                    notifyItemRangeRemoved(position, count);
                }

                @Override
                public void onMoved(int fromPosition, int toPosition) {
                    notifyItemMoved(fromPosition, toPosition);
                }

                @Override
                public int compare(HomePostListItem a, HomePostListItem b) {
                    return COMPARATOR.compare(a, b);
                }

                @Override
                public void onChanged(int position, int count) {
                    notifyItemRangeChanged(position, count);
                }

                @Override
                public boolean areContentsTheSame(HomePostListItem oldItem,
                                                  HomePostListItem newItem) {
                    return oldItem.equals(newItem);
                }

                @Override
                public boolean areItemsTheSame(HomePostListItem item1,
                                               HomePostListItem item2) {
                    return item1 == item2;
                }
            });

    public void replaceAll(ArrayList<HomePostListItem> filteredList) {
        sortedList.clear();
        sortedList.beginBatchedUpdates();

        for (int i = sortedList.size() - 1; i >= 0; i--) {
            final HomePostListItem model = sortedList.get(i);
            if (!filteredList.contains(model)) {
                filteredList.remove(model);
            }
        }

        sortedList.addAll(filteredList);

        notifyDataSetChanged();
        sortedList.endBatchedUpdates();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image_grid_view)
        AppCompatImageView imageGridView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(HomePostListItem homePostListItem) {
            Glide.with(itemView).asBitmap().load(homePostListItem.imageUrl).into(imageGridView);
            PhotoViewAttacher photoViewAttacher = new PhotoViewAttacher(imageGridView);
            photoViewAttacher.update();
        }
    }
}
