package com.hsnozan.instademo.scenes.home.scene.comments.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hsnozan.instademo.R;
import com.hsnozan.instademo.scenes.home.scene.comments.model.CommentsModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ozanal on 2019-05-25
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {

    private ArrayList<CommentsModel> postComments = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.view_comment_list_item, viewGroup,
                        false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.bind(postComments.get(i));
    }

    @Override
    public int getItemCount() {
        return postComments.size();
    }

    public void setCommentArrayList(ArrayList<CommentsModel> postComments) {
        this.postComments = postComments;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_commented_user_description)
        AppCompatTextView commentedUserDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(CommentsModel commentsModel) {
            if (commentsModel != null) {
                if (commentsModel.commentUserName == null) {
                    commentsModel.commentUserName = "denemeUser";
                }
                String userName = "<b>" + commentsModel.commentUserName + "</b>";
                String fullDescription = userName + " " + commentsModel.commentText;
                commentedUserDescription.setText(Html.fromHtml(fullDescription));
            }
        }
    }
}
