package com.hsnozan.instademo.scenes.login.signin;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.hsnozan.instademo.R;
import com.hsnozan.instademo.main.MainActivity;
import com.hsnozan.instademo.scenes.home.activity.HomeActivity;
import com.hsnozan.instademo.scenes.login.signup.SignUpFragment;
import com.hsnozan.instademo.util.FragmentUtil;
import com.hsnozan.instademo.util.GTFrameLayout;
import com.hsnozan.instademo.util.Util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    @BindView(R.id.frame_layout_sign_in)
    GTFrameLayout rootView;
    @BindView(R.id.edit_text_mail)
    AppCompatEditText emailEditText;
    @BindView(R.id.edit_text_password)
    AppCompatEditText passwordEditText;
    @BindView(R.id.button_login)
    AppCompatButton loginButton;

    private FirebaseAuth mAuth;
    private String email = "";
    private String password = "";

    public LoginFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, view);
        mAuth = FirebaseAuth.getInstance();

        return view;
    }


    @OnClick(R.id.button_login)
    public void onLoginButtonClick() {
        boolean isTextsValid =  checkForTextFields();

        if (isTextsValid) {
            rootView.showProgress();
            loginFunc();
        } else {
            Toast.makeText(getContext(), "Check again for email or password", Toast.LENGTH_LONG).show();
        }
    }

    private void loginFunc() {
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(getActivity(), task -> {
            if (task.isSuccessful()) {
                MainActivity activity = (MainActivity) getActivity();
                if (activity != null) {
                    Intent intent = new Intent(activity, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    rootView.hideProgress();
                    Util.closeSoftKeyboard(activity);
                    activity.finish();
                }
            } else {
                rootView.hideProgress();
                Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean checkForTextFields() {
        if (emailEditText.getText() != null) {
            email = emailEditText.getText().toString();
        }
        if (passwordEditText.getText() != null) {
            password = passwordEditText.getText().toString();
        }

        return !email.equals("") && Util.isEmailValid(email);
    }

    @OnClick(R.id.text_sign_up)
    public void onTextSignUpClick() {
        MainActivity activity = (MainActivity) getActivity();

        if (activity != null) {
            SignUpFragment signUpFragment = SignUpFragment.newInstance();
            FragmentUtil.startFragment(activity, activity.getContainer(), signUpFragment);
        }
    }

}
