package com.hsnozan.instademo.scenes.home.scene.homepage;


import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hsnozan.instademo.R;
import com.hsnozan.instademo.main.MainActivity;
import com.hsnozan.instademo.scenes.home.activity.HomeActivity;
import com.hsnozan.instademo.scenes.home.scene.comments.model.CommentsModel;
import com.hsnozan.instademo.scenes.home.scene.homepage.adapter.HomeAdapter;
import com.hsnozan.instademo.scenes.home.scene.homepage.listener.HomeItemCommentListener;
import com.hsnozan.instademo.scenes.home.scene.homepage.model.HomePostListItem;
import com.hsnozan.instademo.scenes.home.scene.comments.CommentsFragment;
import com.hsnozan.instademo.util.FragmentUtil;
import com.hsnozan.instademo.util.GTFrameLayout;
import com.hsnozan.instademo.util.Util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;

import static android.support.constraint.Constraints.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements ValueEventListener, HomeItemCommentListener {

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    @BindView(R.id.frame_layout_home)
    GTFrameLayout rootView;
    @BindView(R.id.recycler_view_home)
    RecyclerView homeRecyclerView;

    private LinearLayoutManager homeLayoutManager;
    private HomeAdapter homeAdapter;
    private DatabaseReference reference;
    private Bitmap bitmap;

    public HomeFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        setAdapter();
        setFirebaseDatabase();
        return view;
    }

    private void setAdapter() {
        homeAdapter = new HomeAdapter();
        homeAdapter.setListener(this);
        homeRecyclerView.setAdapter(homeAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        homeRecyclerView.setLayoutManager(linearLayoutManager);
    }

    private void setFirebaseDatabase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        reference = database
                .getReferenceFromUrl("https://instademo-2409f.firebaseio.com//postList/postListItem");
        rootView.showProgress();
        reference.addValueEventListener(this);
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<HomePostListItem> postList = new ArrayList<>();

        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            HomePostListItem postListItem = snapshot.getValue(HomePostListItem.class);
            if (postListItem != null) {
                postListItem.setPostID(snapshot.getKey());
            }
            postList.add(postListItem);
        }

        homeAdapter.setPostListArrayList(postList);
        homeRecyclerView.scrollToPosition(postList.size() - 1);
        rootView.hideProgress();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {
        Toast.makeText(getContext(), "" + databaseError, Toast.LENGTH_SHORT).show();
        rootView.hideProgress();
    }

    @Override
    public void onCommentButtonClick(HomePostListItem listItem) {
        HomeActivity activity = (HomeActivity) getActivity();

        if (activity != null) {
            CommentsFragment commentsFragment = CommentsFragment.newInstance(listItem);
            commentsFragment.setDatabaseRef(reference);
            FragmentUtil.startFragment(activity, activity.getContainer(), commentsFragment);
        }
    }

    @Override
    public void onSaveButtonClick(Bitmap bitmap) {
        this.bitmap = bitmap;
        checkForPermissions();
    }

    private void checkForPermissions() {
        String[] permissions;
        permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        if (!hasPermissions(getContext(), permissions)) {
            onCameraRequested();
        } else {
            requestCamera();
        }
    }

    private boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission)
                        != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void onCameraRequested() {
        if (getActivity() == null) {
            Log.w(TAG, "onCameraRequested: context is null");
            return;
        }

        String[] perms;
        perms = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        if (EasyPermissions.hasPermissions(getActivity(), perms)) {
            requestCamera();
        } else {
            EasyPermissions.requestPermissions(new PermissionRequest
                    .Builder(this, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE, perms)
                    .setRationale(R.string.text_need_write_storage_access)
                    .setPositiveButtonText(R.string.text_ok)
                    .setNegativeButtonText(R.string.text_cancel)
                    .build());
        }
    }

    @AfterPermissionGranted(MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE)
    private void requestCamera() {
        HomeActivity activity = (HomeActivity) getActivity();
        if (activity != null) {
            downloadPhoto();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE) {
            downloadPhoto();
        }
    }

    private void downloadPhoto() {
        MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap, "IMG", "");
        Toast.makeText(getContext(), "Photo downloaded successfully", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.image_log_out_button)
    public void onLogOutButtonClick() {
        HomeActivity activity = (HomeActivity) getActivity();

        if (activity != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.text_popup_title);
            builder.setMessage(R.string.text_logout_detail);
            builder.setNegativeButton(R.string.text_no, null);
            builder.setPositiveButton(R.string.text_yes, (dialogInterface, i) -> {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(activity, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                Util.closeSoftKeyboard(activity);
                activity.finish();

            });
            builder.show();
        }
    }
}
