package com.hsnozan.instademo.scenes.login.signup;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hsnozan.instademo.R;
import com.hsnozan.instademo.main.MainActivity;
import com.hsnozan.instademo.scenes.login.model.UserModel;
import com.hsnozan.instademo.util.FragmentUtil;
import com.hsnozan.instademo.util.GTFrameLayout;
import com.hsnozan.instademo.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment {

    @BindView(R.id.frame_layout_sign_up)
    GTFrameLayout rootView;
    @BindView(R.id.edit_text_sign_up_username)
    AppCompatEditText userNameEditText;
    @BindView(R.id.edit_text_sign_up_mail)
    AppCompatEditText mailEditText;
    @BindView(R.id.edit_text_sign_up_password)
    AppCompatEditText passwordEditText;

    private FirebaseAuth firebaseAuth;
    private String userName;
    private String mail;
    private String password;
    private DatabaseReference reference;

    public SignUpFragment() {
        // Required empty public constructor
    }

    public static SignUpFragment newInstance() {
        return new SignUpFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        ButterKnife.bind(this, view);
        firebaseAuth = FirebaseAuth.getInstance();
        setUpFirebaseDatabase();
        return view;
    }

    private void setUpFirebaseDatabase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        reference = database.getReferenceFromUrl("https://instademo-2409f.firebaseio.com//userList/user");
    }

    @OnClick(R.id.button_sign_up)
    public void onSignUpButtonClick() {
        userName = userNameEditText.getText().toString();
        mail = mailEditText.getText().toString();
        password = passwordEditText.getText().toString();

        if (userName.equals("") || mail.equals("") || password.equals("")) {
            Toast.makeText(getContext(), "Can not be empty", Toast.LENGTH_SHORT).show();
        } else if (password.length() < 6) {
            Toast.makeText(getContext(), "Password can not be less than 6 character", Toast.LENGTH_SHORT).show();
        } else {
            rootView.showProgress();
            checkForInputs();
        }
    }

    private void checkForInputs() {
        if (Util.isEmailValid(mail)) {
            firebaseAuth.createUserWithEmailAndPassword(mail, password).addOnCompleteListener(getActivity(), task -> {
                if (task.isSuccessful()) {
                    if (firebaseAuth.getCurrentUser() != null) {
                        createUser(firebaseAuth.getCurrentUser());
                    }
                } else {
                    rootView.hideProgress();
                    Toast.makeText(getContext(), "Somethings got wrong", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void createUser(FirebaseUser currentUser) {
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(userName)
                .build();
        UserModel userModel = new UserModel();
        userModel.userName = userName;
        reference.push().setValue(userModel);
        currentUser.updateProfile(profileUpdates).addOnCompleteListener(task -> {
            MainActivity activity = (MainActivity) getActivity();

            if (activity != null) {
                rootView.hideProgress();
                Util.closeSoftKeyboard(activity);
                FragmentUtil.popLatestFragment(activity);
            }
        });
    }

}
