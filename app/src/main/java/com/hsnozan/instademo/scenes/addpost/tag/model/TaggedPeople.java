package com.hsnozan.instademo.scenes.addpost.tag.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ozanal on 2019-05-26
 */
public class TaggedPeople implements Parcelable {

    public int x;
    public int y;
    public String taggedPerson;

    public TaggedPeople() {

    }

    protected TaggedPeople(Parcel in) {
        x = in.readInt();
        y = in.readInt();
        taggedPerson = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(x);
        dest.writeInt(y);
        dest.writeString(taggedPerson);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TaggedPeople> CREATOR = new Creator<TaggedPeople>() {
        @Override
        public TaggedPeople createFromParcel(Parcel in) {
            return new TaggedPeople(in);
        }

        @Override
        public TaggedPeople[] newArray(int size) {
            return new TaggedPeople[size];
        }
    };
}
