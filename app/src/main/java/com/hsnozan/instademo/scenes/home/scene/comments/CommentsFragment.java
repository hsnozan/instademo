package com.hsnozan.instademo.scenes.home.scene.comments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.hsnozan.instademo.R;
import com.hsnozan.instademo.scenes.home.activity.HomeActivity;
import com.hsnozan.instademo.scenes.home.scene.comments.adapter.CommentsAdapter;
import com.hsnozan.instademo.scenes.home.scene.comments.model.CommentsModel;
import com.hsnozan.instademo.scenes.home.scene.homepage.model.HomePostListItem;
import com.hsnozan.instademo.util.FragmentUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommentsFragment extends Fragment {

    private static final String KEY_HOME_POST_MODEL = "KEY_HOME_POST_MODEL";

    @BindView(R.id.text_user_description)
    AppCompatTextView userDescription;
    @BindView(R.id.recycler_view_comments)
    RecyclerView commentsRecyclerView;
    @BindView(R.id.edit_text_post_comment)
    AppCompatEditText nextCommentEditText;

    private CommentsAdapter commentsAdapter;
    private DatabaseReference reference;
    private ArrayList<CommentsModel> commentsArrayList = new ArrayList<>();
    private HomePostListItem homePostListItem;
    private FirebaseAuth firebaseAuth;
    private int count = 0;

    public static CommentsFragment newInstance(HomePostListItem listItem) {
        CommentsFragment commentsFragment = new CommentsFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_HOME_POST_MODEL, listItem);
        commentsFragment.setArguments(args);

        return commentsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comments, container, false);
        ButterKnife.bind(this, view);
        firebaseAuth = FirebaseAuth.getInstance();
        setAdapter();

        if (getArguments() != null && getArguments().getParcelable(KEY_HOME_POST_MODEL) != null) {
            homePostListItem = getArguments().getParcelable(KEY_HOME_POST_MODEL);
            setUpView(homePostListItem);
        }

        return view;
    }

    private void setAdapter() {
        commentsAdapter = new CommentsAdapter();
        commentsRecyclerView.setAdapter(commentsAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        commentsRecyclerView.setLayoutManager(linearLayoutManager);
    }

    private void setUpView(HomePostListItem homePostListItem) {
        String userName = "<b>" + homePostListItem.userName + "</b>";
        String fullDescription = userName + " " + homePostListItem.postDescription;
        userDescription.setText(Html.fromHtml(fullDescription));
        if (homePostListItem.postComments != null) {
            commentsArrayList = homePostListItem.postComments;
            commentsAdapter.setCommentArrayList(commentsArrayList);
            count = commentsArrayList.size();
        }
    }

    @OnClick(R.id.button_post_comment)
    public void onPostButtonClick() {
        String nextCommentText = nextCommentEditText.getText().toString();

        if (!nextCommentText.equals("")) {
            CommentsModel commentsModel = new CommentsModel();
            commentsModel.commentUserName = firebaseAuth.getCurrentUser().getDisplayName();
            commentsModel.commentText = nextCommentText;
            commentsArrayList.add(commentsModel);
            commentsAdapter.setCommentArrayList(commentsArrayList);
            pushCommentToFirebase(commentsModel);
            nextCommentEditText.getText().clear();
        }
    }

    private void pushCommentToFirebase(CommentsModel commentsModel) {
        DatabaseReference newCommentReference = reference.child(homePostListItem.getPostID() + "/postComments");
        newCommentReference.child(String.valueOf(count)).setValue(commentsModel);
    }

    @OnClick(R.id.button_comments_back)
    public void onBackButtonClick() {
        HomeActivity activity = (HomeActivity) getActivity();

        if (activity != null) {
            FragmentUtil.popLatestFragment(activity);
        }

    }

    public void setDatabaseRef(DatabaseReference reference) {
        this.reference = reference;
    }
}
