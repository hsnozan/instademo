package com.hsnozan.instademo.scenes.addpost.tag.listener;

/**
 * Created by ozanal on 2019-05-25
 */
public interface UserItemClickListener {

    void onUserItemClickListener(String userName);

}
