package com.hsnozan.instademo.scenes.explore;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.hsnozan.instademo.R;
import com.hsnozan.instademo.scenes.explore.adapter.ExploreAdapter;
import com.hsnozan.instademo.scenes.home.scene.homepage.adapter.HomeAdapter;
import com.hsnozan.instademo.scenes.home.scene.homepage.model.HomePostListItem;
import com.hsnozan.instademo.util.Util;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExploreFragment extends Fragment implements ValueEventListener, TextWatcher {

    @BindView(R.id.recycler_view_grid_explore)
    RecyclerView gridView;
    @BindView(R.id.edit_text_explore_filter)
    AppCompatEditText exploreEdittext;

    private ExploreAdapter exploreAdapter;
    private DatabaseReference reference;
    private ArrayList<HomePostListItem> exploreArrayList = new ArrayList<>();

    public ExploreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        ButterKnife.bind(this, view);

        setAdapter();
        setFirebaseDatabase();
        exploreEdittext.addTextChangedListener(this);
        return view;
    }

    private void setAdapter() {
        exploreAdapter = new ExploreAdapter();
        gridView.setAdapter(exploreAdapter);
        int numberOfColumns = Util.calculateNoOfColumns(getContext(), 140);
        gridView.setLayoutManager(new GridLayoutManager(getContext(), numberOfColumns));
    }

    private void setFirebaseDatabase() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        reference = database
                .getReferenceFromUrl("https://instademo-2409f.firebaseio.com//postList/postListItem");
        reference.addValueEventListener(this);
    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        ArrayList<HomePostListItem> postList = new ArrayList<>();

        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            HomePostListItem postListItem = snapshot.getValue(HomePostListItem.class);
            if (postListItem != null) {
                postListItem.setPostID(snapshot.getKey());
            }
            postList.add(postListItem);
        }

        exploreArrayList = postList;
        exploreAdapter.replaceAll(postList);
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (!charSequence.toString().equals("")) {
            onFilterQueryChange(charSequence.toString());
        } else {
            exploreAdapter.replaceAll(exploreArrayList);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private void onFilterQueryChange(String newQuery) {
        final ArrayList<HomePostListItem> filteredModelList = filter(newQuery);
        exploreAdapter.replaceAll(filteredModelList);
    }

    private ArrayList<HomePostListItem> filter(String query) {
        Locale locale = new Locale("tr", "TR");
        final String lowerCaseQuery = query.toLowerCase(locale);
        final ArrayList<HomePostListItem> filteredList = new ArrayList<>();

        for (HomePostListItem userModel : exploreArrayList) {
            final String text = userModel
                    .userName.toLowerCase(locale);
            final String imageUrl = userModel.imageUrl;

            if (text.contains(lowerCaseQuery)) {
                HomePostListItem homePostListItem
                        = new HomePostListItem();
                homePostListItem.userName = text;
                homePostListItem.imageUrl = imageUrl;
                filteredList.add(homePostListItem);
            }
        }

        return filteredList;
    }
}
