package com.hsnozan.instademo.util;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Log;

import com.hsnozan.instademo.R;
import com.hsnozan.instademo.util.helper.GenericFileProvider;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public final class ImagePicker {

    private ImagePicker() {

    }

    private static final String TAG = "ImagePicker";
    private static final String TEMP_IMAGE_NAME = "tempImage";

    private static final int IMAGE_WIDTH_MAX = 1440;
    private static final boolean RESIZE_IMAGE = true;

    private static final int DEGREE_270 = 270;
    private static final int DEGREE_360 = 360;
    private static final int DEGREE_90 = 90;
    private static final int DEGREE_180 = 180;
    private static final int QUALITY_90 = 90;
    private static final int QUALITY_100 = 100;
    private static final int SAMPLE_SIZE_5 = 5;
    private static final int SAMPLE_SIZE_3 = 3;

    public static Intent getPickImageIntent(Context context) {
        Intent chooserIntent = null;

        List<Intent> intentList = new ArrayList<>();

        Intent pickIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra("return-data", true);

        Uri apkURI = GenericFileProvider.getUriForFile(
                context, context.getApplicationContext().getPackageName()
                        + ".util.helper.GenericFileProvider",
                getTempFile(context));
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, apkURI);
        takePhotoIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        intentList = addIntentsToList(context, intentList, pickIntent);
        intentList = addIntentsToList(context, intentList, takePhotoIntent);

        if (intentList.size() > 0) {
            chooserIntent = Intent.createChooser(intentList.remove(intentList.size() - 1),
                    context.getString(R.string.text_image_picker));
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
                    intentList.toArray(new Parcelable[]{}));
        }

        return chooserIntent;
    }

    private static File getTempFile(Context context) {
        File imageFile = new File(context.getExternalCacheDir(), TEMP_IMAGE_NAME);
        imageFile.getParentFile().mkdirs();

        return imageFile;
    }

    private static List<Intent> addIntentsToList(Context context, List<Intent> list,
                                                 Intent intent) {
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent,
                0);

        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
            Log.d(TAG, "Intent: " + intent.getAction() + " package: " + packageName);
        }

        return list;
    }

    public static Uri getImageFromResult(Context context, Intent imageReturnedIntent) {
        Uri selectedImageUri = null;
        Uri rotatedImageUri = null;
        File imageFile = getTempFile(context);

        boolean isCamera = (imageReturnedIntent == null
                || imageReturnedIntent.getData() == null
                || imageReturnedIntent.getData().toString().contains(imageFile.toString()));

        if (isCamera) {     /** CAMERA **/
            selectedImageUri = Uri.fromFile(imageFile);
        } else {            /** ALBUM **/
            selectedImageUri = imageReturnedIntent.getData();
        }

        try {
            int rotation = getRotation(context, selectedImageUri, isCamera);
            Bitmap image = RESIZE_IMAGE ? getImageResized(context, selectedImageUri)
                    : MediaStore.Images.Media.getBitmap(context.getContentResolver(),
                    selectedImageUri);

            image = rotate(image, rotation);
            rotatedImageUri = getImageUri(context, image);
        } catch (Exception e) {
            e.printStackTrace();
            rotatedImageUri = selectedImageUri;
        }

        return rotatedImageUri;
    }

    private static int getRotation(Context context, Uri imageUri, boolean isCamera) {
        int rotation;

        if (isCamera) {
            rotation = getRotationFromCamera(context, imageUri);
        } else {
            rotation = getRotationFromGallery(context, imageUri);
        }

        Log.d(TAG, "Image rotation: " + rotation);

        return rotation;
    }

    /**
     * Resize to avoid using too much memory loading big images (e.g.: 2560*1920)
     **/
    private static Bitmap getImageResized(Context context, Uri selectedImage) {
        Bitmap bm = null;
        int[] sampleSizes = new int[]{SAMPLE_SIZE_5, SAMPLE_SIZE_3, 2, 1};
        int i = 0;

        do {
            bm = decodeBitmap(context, selectedImage, sampleSizes[i]);
            Log.d(TAG, "resizer: new bitmap width = " + bm.getWidth());
            i++;
        }
        while (Math.min(bm.getWidth(), bm.getHeight()) < IMAGE_WIDTH_MAX && i < sampleSizes.length);

        return bm;
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),
                matrix, true);
    }

    public static Uri getImageUri(Context context, Bitmap image) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, QUALITY_100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), image,
                "Title", null);

        return Uri.parse(path);
    }

    private static int getRotationFromCamera(Context context, Uri imageFile) {
        int rotate = 0;

        try {
            context.getContentResolver().notifyChange(imageFile, null);
            ExifInterface exif = new ExifInterface(imageFile.getPath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = DEGREE_270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = DEGREE_180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = DEGREE_90;
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rotate;
    }

    public static int getRotationFromGallery(Context context, Uri imageUri) {
        int result = 0;
        String[] columns = {MediaStore.Images.Media.ORIENTATION};
        Cursor cursor = null;

        try {
            cursor = context.getContentResolver().query(imageUri, columns, null,
                    null, null);
            if (cursor != null && cursor.moveToFirst()) {
                int orientationColumnIndex = cursor.getColumnIndex(columns[0]);
                result = cursor.getInt(orientationColumnIndex);
            }
        } catch (Exception e) {
            //Do nothing
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return result;
    }

    private static Bitmap decodeBitmap(Context context, Uri theUri, int sampleSize) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = sampleSize;
        AssetFileDescriptor fileDescriptor = null;

        try {
            fileDescriptor = context.getContentResolver().openAssetFileDescriptor(theUri,
                    "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Bitmap actuallyUsableBitmap = BitmapFactory.decodeFileDescriptor(
                fileDescriptor.getFileDescriptor(), null, options);

        Log.d(TAG, options.inSampleSize + " sample method bitmap ... "
                + actuallyUsableBitmap.getWidth() + " " + actuallyUsableBitmap.getHeight());

        return actuallyUsableBitmap;
    }

    private static void rotate(Context context, Uri uri, int rotation) throws IOException {
        if (rotation != 0) {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            Matrix matrix = new Matrix();
            matrix.postRotate(rotation);
            Bitmap bmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);
            OutputStream os = context.getContentResolver().openOutputStream(uri);
            bmp.compress(Bitmap.CompressFormat.PNG, QUALITY_90, os);
        }
    }

    public static Bitmap modifyOrientation(Bitmap bitmap, String imageAbsolutePath)
            throws IOException {
        ExifInterface ei = new ExifInterface(imageAbsolutePath);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, DEGREE_90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, DEGREE_180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, DEGREE_270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);

        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(),
                matrix, true);
    }

    public static Uri saveToInternalStorage(Context context, Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(context);
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        File imagePath = new File(directory, "img.jpg");
        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream(imagePath);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, QUALITY_100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return Uri.fromFile(imagePath);
    }

}
