package com.hsnozan.instademo.util.view;

import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by ozanal on 2019-05-23
 */
public class TabLayoutTabView extends android.support.v7.widget.AppCompatImageView {

    public TabLayoutTabView(Context context) {
        super(context);
    }

    public TabLayoutTabView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TabLayoutTabView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setIcons(int mIconId, int mIconIdSelected) {
        StateListDrawable res = new StateListDrawable();
        res.setExitFadeDuration(300);
        res.addState(new int[]{android.R.attr.state_selected}, getResources().getDrawable(mIconIdSelected));
        res.addState(new int[]{}, getResources().getDrawable(mIconId));

        setBackground(res);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(30, 20, 30, 0);
        setLayoutParams(layoutParams);
    }

    public void updateIcons(int mIconId, int mIconIdSelected) {
        StateListDrawable res = new StateListDrawable();
        res.addState(new int[]{android.R.attr.state_selected}, getResources().getDrawable(mIconIdSelected));
        res.addState(new int[]{}, getResources().getDrawable(mIconId));

        setBackground(res);
    }
}
