package com.hsnozan.instademo.util;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.List;

/**
 * Created by ozanal on 2019-05-23
 */
public class FragmentUtil {


    private FragmentUtil() {

    }

    public static void startFragment(AppCompatActivity activity, int container, Fragment fragment) {
        FragmentManager manager = activity.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(container, fragment);
        transaction.addToBackStack(fragment.getTag());
        transaction.commitAllowingStateLoss();
    }

    public static void popLatestFragment(AppCompatActivity activity, Fragment fragment) {
        if (activity != null && fragment != null) {
            try {
                activity.getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            } catch (Exception e) {
                Log.i("BackStackCatch", "popBackStack can not handled");
            }
        }
    }

    public static void popLatestFragment(AppCompatActivity activity) {
        if (activity != null) {
            try {
                activity.getSupportFragmentManager().popBackStackImmediate();
            } catch (Exception e) {
                Log.i("BackStackCatch", "popBackStack can not handled");
            }
        }
    }

    public static void clearFragmentStack(AppCompatActivity activity) {
        if (activity != null) {
            FragmentManager fragmentManager = activity.getSupportFragmentManager();
            clearFragmentStack(fragmentManager);
        }
    }

    public static void clearFragmentStack(FragmentManager fragmentManager) {
        Integer stackCount = fragmentManager.getBackStackEntryCount();

        if (stackCount != null) {
            for (int i = 0; i < stackCount; ++i) {
                try {
                    fragmentManager.popBackStack();
                } catch (Exception e) {
                    Log.i("BackStackCatch", "popBackStack can not handled");
                }
            }
        }
    }

    public static boolean isVisibleToUser(AppCompatActivity activity, Class fragmentClass) {
        if (activity == null) {
            return false;
        }

        List<Fragment> fragments = activity.getSupportFragmentManager().getFragments();

        return !fragments.isEmpty()
                && fragmentClass.isInstance(fragments.get(fragments.size() - 1));
    }
}
