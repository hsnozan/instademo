package com.hsnozan.instademo.util.view;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.hsnozan.instademo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ozanal on 2019-05-24
 */
public class HomeNavigationView extends LinearLayout {

    private static final int MINIMUM_ACTION_DELAY = 1000;
    public static final int POSITION_FIRST = 0;
    public static final int POSITION_SCAN = 1;
    public static final int POSITION_LAST = 2;

    @BindView(R.id.tabitem_first)
    AppCompatTextView firstTabItem;
    @BindView(R.id.tabitem_scan)
    AppCompatTextView scanTabItem;
    @BindView(R.id.tabitem_last)
    AppCompatTextView lastTabItem;

    private OnItemSelectionListener itemSelectionListener;
    private long mLastActionTime;
    private int selectedItemPosition = POSITION_FIRST;

    public HomeNavigationView(Context context) {
        super(context);

        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.view_home_navigation, this);
        LayoutParams layoutParams =
                new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(layoutParams);

        ButterKnife.bind(this, view);

        firstTabItem.callOnClick();
    }

    public HomeNavigationView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public HomeNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    @OnClick(R.id.tabitem_first)
    public void onFirstTabItemClick() {
        onItemClick(POSITION_FIRST);
    }

    public void onItemClick(int itemPosition) {
        firstTabItem.setSelected(itemPosition == POSITION_FIRST || (itemPosition == POSITION_SCAN
                && selectedItemPosition == POSITION_FIRST));
        scanTabItem.setSelected(itemPosition == POSITION_SCAN);
        lastTabItem.setSelected(itemPosition == POSITION_LAST || (itemPosition == POSITION_SCAN
                && selectedItemPosition == POSITION_LAST));

        if (itemSelectionListener != null) {
            itemSelectionListener.onItemSelected(itemPosition);
        }

        if (itemPosition != POSITION_SCAN) {
            selectedItemPosition = itemPosition;
        }
    }

    @OnClick(R.id.tabitem_scan)
    public void onScanClick() {
        onItemClick(POSITION_SCAN);
    }

    @OnClick(R.id.tabitem_last)
    public void onLastTabItemClick() {
        onItemClick(POSITION_LAST);
    }

    public boolean onBackPressed() {
        if (selectedItemPosition != POSITION_FIRST) {
            onFirstTabItemClick();
            return true;
        }

        return false;
    }

    public void setItemSelectionListener(OnItemSelectionListener itemSelectionListener) {
        this.itemSelectionListener = itemSelectionListener;
    }

    public interface OnItemSelectionListener {

        void onItemSelected(int itemIndex);
    }

}