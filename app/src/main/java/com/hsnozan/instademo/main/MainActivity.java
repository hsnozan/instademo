package com.hsnozan.instademo.main;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.hsnozan.instademo.R;
import com.hsnozan.instademo.scenes.login.signin.LoginFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager manager = getSupportFragmentManager();
        LoginFragment loginFragment = (LoginFragment) manager.findFragmentById(getContainer());

        if (loginFragment == null) {
            loginFragment = new LoginFragment();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(getContainer(), loginFragment);
            transaction.commit();
        }
    }

    public int getContainer() {
        return R.id.main_container;
    }
}
